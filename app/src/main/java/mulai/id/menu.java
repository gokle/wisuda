package mulai.id;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void sila(View view) {
        Intent intent = new Intent(menu.this, pancasila.class);
        startActivity(intent);

        Toast.makeText(menu.this,"halaman Pancasila", Toast.LENGTH_SHORT).show();
    }

    public void tri(View view) {
        Intent intent = new Intent(menu.this, TriSatya.class);
        startActivity(intent);

        Toast.makeText(menu.this,"halaman Tri Satya", Toast.LENGTH_SHORT).show();
    }

    public void dasa(View view) {
        Intent intent = new Intent(menu.this, DasaDharma.class);
        startActivity(intent);

        Toast.makeText(menu.this,"halaman Dasa Dharma", Toast.LENGTH_SHORT).show();
    }

    public void ditempat(View view) {
        Intent intent = new Intent(menu.this, AbaAbaDitempat.class);
        startActivity(intent);

        Toast.makeText(menu.this,"Aba aba ditempat", Toast.LENGTH_SHORT).show();
    }

    public void jalan(View view) {
        Intent intent = new Intent(menu.this, AbaAbaJalan.class);
        startActivity(intent);

        Toast.makeText(getApplicationContext(),"Aba aba jalan", Toast.LENGTH_SHORT).show();

    }
    public void kuis(View view) {
        Intent intent = new Intent(menu.this, Kuis.class);
        startActivity(intent);
        finish();
        Toast.makeText(getApplicationContext(),"mulai kuis", Toast.LENGTH_SHORT).show();

    }

    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Apakah kamu mau keluar aplikasi?");
        builder.setPositiveButton("ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
            }
        });
        builder.setNegativeButton("tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}