package mulai.id;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Kuis extends AppCompatActivity {

    private TextView soal,nilai;
    private Button nextsoal, barumulai;
    public RadioGroup btngroup;
    public RadioButton tombol1, tombol2, tombol3;
    final int min = 1;
    final int max = 20;
    public int patokan ;
    public int benar = 0;
    int number_random;
    int batasan = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuis);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        btngroup = findViewById(R.id.radioGroup);
        barumulai = findViewById(R.id.mulai);
        soal = findViewById(R.id.textsoal);
        nextsoal = findViewById(R.id.nextsoal);
        tombol1 = findViewById(R.id.jawaban1);
        tombol2 = findViewById(R.id.jawaban2);
        tombol3 = findViewById(R.id.jawaban3);
        nilai = findViewById(R.id.poin);



        barumulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soal.setVisibility(View.VISIBLE);
                btngroup.setVisibility(View.VISIBLE);
                nextsoal.setVisibility(View.VISIBLE);
                barumulai.setVisibility(View.INVISIBLE);

                // random AI + nampilin soal


                pilihan();

            }
        });

        nextsoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //menentukan patokan jawaban
                //--------------------------------------------
                if (patokan == 1) {
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol1.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol2.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 2){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol2.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 3){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol3.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol2.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 4){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol2.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 5){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol1.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol2.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 6){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol1.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol2.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 7){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol2.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 8){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol3.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol2.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 9){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol2.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 10){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol3.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol2.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 11){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol2.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 12){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol2.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 13){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol3.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol2.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 14){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol1.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol2.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 15){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol1.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol2.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 16){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol3.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol2.isChecked() || tombol1.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 17){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol2.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 18){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol1.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol2.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 19){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol2.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol1.isChecked() || tombol3.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }
                else if (patokan == 20){
                    // menentukan jawaban jawaban keberapa yg benar
                    if (tombol3.isChecked()) {
                        benar += 10;
                        ShowToast("pilhan benar");
                        pilihan();
                    } else if (tombol2.isChecked() || tombol1.isChecked()) {
                        ShowToast("pilhan tidak benar");
                        pilihan();
                    } else {
                        ShowToast("sepertinya anda belum memilih apapun");
                    }
                }

                nilai.setText(String.valueOf(benar));
            }

            public void ShowToast(String s) {
                Toast toast = Toast.makeText(getApplication(), s, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER,0,0);
                toast.show();

            }
        });

    }

    public void pilihan() {

        //untuk batasan soal yang tampil dari 20 soal yang ada
        if (batasan <= 10){
            number_random = new Random().nextInt((max - min) + 1) + min;
            // soal.setText(String.valueOf(number_random)); //kalau soal kosong hanya menampilkan angka random saja
            patokan = number_random;

            if (patokan == 1) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal1);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda1);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda13);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda17);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==2){
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal2);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda3);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda2);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda18);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==3){
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal3);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda13);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda8);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda3);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==4) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal4);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda6);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda4);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda11);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==5) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal5);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda5);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda20);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda15);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==6) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal6);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda6);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda19);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda15);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==7) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal7);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda15);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda7);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda15);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==8) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal8);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda12);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda2);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda8);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==9) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal9);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda1);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda9);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda15);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==10) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal10);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda17);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda14);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda10);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==11) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal11);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda19);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda11);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda19);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==12) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal12);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda3);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda12);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda1);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==13) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal13);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda8);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda10);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda13);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==14) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal14);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda14);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda19);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda1);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==15) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal15);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda15);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda6);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda7);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==16) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal16);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda10);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda13);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda16);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==17) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal17);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda18);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda17);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda10);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==18) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal18);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda18);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda10);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda8);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==19) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal19);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda5);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda19);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda20);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }
            else if (patokan==20) {
                // ini untuk meletakkan string Resource di layout soal dan pil ganda
                String soal1 = getString(R.string.soal20);
                soal.setText(soal1);
                String jwb1 = getString(R.string.ganda7);
                tombol1.setText(jwb1);
                String jwb2 = getString(R.string.ganda7);
                tombol2.setText(jwb2);
                String jwb3 = getString(R.string.ganda20);
                tombol3.setText(jwb3);
                // jika tombol yang benar terpilih
            }

            batasan++;
        }else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setTitle("Nilai kamu adalah : " +benar);
            builder.setMessage("Apakah kamu ingin keluar dari kuis ?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //if user pressed "yes", then he is allowed to exit from application
                    Intent intent = new Intent(Kuis.this, menu.class);
                    startActivity(intent);
                    finish();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

    }
    public void onBackPressed() {
        //ini cuman buat disable back button doang!!!!
        Toast toast = Toast.makeText(getApplication(), "Anda belum menyelesaikan permainan, silahkan selesaikan !", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

}