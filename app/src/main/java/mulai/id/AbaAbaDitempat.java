package mulai.id;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

public class AbaAbaDitempat extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aba_aba_ditempat);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void siap(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, SiapGrak.class);
        startActivity(intent);
    }

    public void berhitung(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, Berhitung.class);
        startActivity(intent);
    }

    public void lencangkanan(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, LencangKanan.class);
        startActivity(intent);
    }

    public void setenganlenganlencangkanan(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, SetenganLengan.class);
        startActivity(intent);
    }

    public void hadapkanan(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, HadapKanan.class);
        startActivity(intent);
    }

    public void hadapkiri(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, HadapKiri.class);
        startActivity(intent);
    }

    public void balikkanan(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, BalikKanan.class);
        startActivity(intent);
    }

    public void istirahat(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, IstirahatDitempat.class);
        startActivity(intent);
    }

    public void hormat(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, Hormat.class);
        startActivity(intent);
    }

    public void serongkanan(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, Serongkanan.class);
        startActivity(intent);
    }

    public void serongkiri(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, SerongKiri.class);
        startActivity(intent);
    }

    public void lencangdepan(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, LencangDepan.class);
        startActivity(intent);
    }


    public void hormatbenderanasional(View view) {
        Intent intent = new Intent(AbaAbaDitempat.this, HormatBenderaNasional.class);
        startActivity(intent);
    }
}
