package mulai.id;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends AppCompatActivity {

    private ViewPager screenpager;
    Adaptor adaptor;
    TabLayout tabIndicator;
    Button btnNext;
    int position = 0;
    Button btnGetStarted;
    Animation btnanim;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //make activity on full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow() .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        //when this activity is about to be launch we need to chek if its opened before or not

        if (restorePrefData()){

            Intent mainActivity = new Intent(getApplicationContext(),menu.class);
            startActivity(mainActivity);
            finish();
        }


        setContentView(R.layout.activity_intro);
        //hide the action bar
        getSupportActionBar();

        //view
        btnNext = findViewById(R.id.btnNext);
        btnGetStarted = findViewById(R.id.btn_getstated);
        tabIndicator = findViewById(R.id.tabLayout);
        btnanim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_anim);

        //fill list screen
        final List<Screenitem> mlist=new ArrayList<>();
        mlist.add(new Screenitem("Aba Aba", R.string.gambar1,R.drawable.img1));
        mlist.add(new Screenitem("bahasa", R.string.gambar2,R.drawable.img2));
        mlist.add(new Screenitem("pramuka", R.string.gambar3,R.drawable.img3));


//setup viewpager
        screenpager =findViewById(R.id.screen_viewpager);
        adaptor = new Adaptor(this,mlist);
        screenpager.setAdapter(adaptor);


        //setup tabindikator
        tabIndicator.setupWithViewPager(screenpager);

        //next button
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    position = screenpager.getCurrentItem();
                    if (position < mlist.size()){

                        position++;
                        screenpager.setCurrentItem(position);
                    }

                    if (position == mlist.size()-1){ //when we rech the last screen

                        //TODO : show the GETSTARTED Button and hide the indicator and next button

                        loaddLastScreen();

                    }
            }
        });

        //tablayout add change listener

        tabIndicator.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition()== mlist.size()-1){
                    loaddLastScreen();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        //get started buton listener
        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //pindah menu
                Intent mainActivity = new Intent(getApplicationContext(), menu.class);
                       startActivity(mainActivity);
                       //

                saveprefsData();
                finish();
            }
        });
    }

    private boolean restorePrefData() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs",MODE_PRIVATE);
        boolean isIntroActivityOpenedBefore = pref.getBoolean("isIntroOpened",false);
        return isIntroActivityOpenedBefore;

    }

    private void saveprefsData() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isIntroOpened",true);
        editor.commit();
    }


    //show the GETSTARTED Button and hide the indicator and next button
    private void loaddLastScreen() {
        btnNext.setVisibility(View.INVISIBLE);
        btnGetStarted.setVisibility(View.VISIBLE);
        tabIndicator.setVisibility(View.INVISIBLE);

        //TODO : ADD an animation the getstarted button
        //setup animation
        btnGetStarted.setAnimation(btnanim);
    }
}
