package mulai.id;

public class Screenitem {
    String Title;
    int ScreenImg,Description;

    public Screenitem(String title, int description, int screenImg){
        Title =title;
        Description = description;
        ScreenImg =screenImg;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setDescription(int description) {
        Description = description;
    }

    public void setScreenImg(int screenImg) {
        ScreenImg = screenImg;
    }

    public String getTitle() {
        return Title;
    }

    public int getDescription() {
        return Description;
    }

    public int getScreenImg() {
        return ScreenImg;
    }
}
