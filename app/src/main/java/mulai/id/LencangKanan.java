package mulai.id;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;


public class LencangKanan extends YouTubeBaseActivity {
    private static final String TAG = "SLencang Kanan";
    YouTubePlayerView mYouTubePlayerView;
    Button btnPlay;
    YouTubePlayer.OnInitializedListener mOnInitializedListener;
    MediaController mediaC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lencang_kanan);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Log.d(TAG, "onCreate: Starting.");
        btnPlay = (Button) findViewById(R.id.btnMain);
        mYouTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtubeplay);
        mediaC = new MediaController(this);

        //buat teks suara arab
        final MediaPlayer suaraarab = MediaPlayer.create(this, R.raw.lencangkanan_arb);
        final ImageView SiapaArab = (ImageView) this.findViewById(R.id.lencangkananarab);
        SiapaArab.setEnabled(true);
        SiapaArab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suaraarab.start();
                suaraarab.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        SiapaArab.setEnabled(true);
                    }
                });
                SiapaArab.setEnabled(false);
            }
        });
        //buat teks suara inggris
        final MediaPlayer suarainggris = MediaPlayer.create(this, R.raw.lencangkanan_ing);
        final ImageView SiapaInggris = (ImageView) this.findViewById(R.id.lencangkananinggris);
        SiapaInggris.setEnabled(true);
        SiapaInggris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suarainggris.start();
                suarainggris.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        SiapaInggris.setEnabled(true);
                    }
                });
                SiapaInggris.setEnabled(false);
            }
        });
        // buat youtube
        mOnInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                Log.d(TAG, "onClick: Done Initializing.");

                youTubePlayer.loadVideo("EUEF9IDt2BY");
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Log.d(TAG, "onClick: Failed to Initialize.");
            }
        };
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: Intializing YouTube Player.");
                mYouTubePlayerView.initialize(YouTubeConfig.getApiKey(), mOnInitializedListener);

            }
        });
    }
}