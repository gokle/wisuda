package mulai.id;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AbaAbaJalan extends AppCompatActivity {
    String Ababajalan;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aba_aba_jalan);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void majujalan(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, MajuJalan.class);
        startActivity(intent);
    }

    public void berhenti(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, Berhenti.class);
        startActivity(intent);
    }

    public void belokkanan(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, BelokKanan.class);
        startActivity(intent);
    }

    public void belokkiri(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, BelokKiri.class);
        startActivity(intent);
    }

    public void langkahtegap(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, LangkahTegap.class);
        startActivity(intent);
    }



    public void hormatkanan(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, HormatKanan.class);
        startActivity(intent);
    }

    public void gantilangkah(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, GantiLangkah.class);
        startActivity(intent);
    }

    public void jalanditempat(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, JalanDitempat.class);
        startActivity(intent);
    }


    public void bukabarisan(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, BukaBarisan.class);
        startActivity(intent);
    }

    public void tutupbarisan(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, TutupBarisan.class);
        startActivity(intent);
    }

    public void dualangkahkedepan(View view) {
        Intent intent = new Intent(AbaAbaJalan.this, DuaLangkahKedepan.class);
        startActivity(intent);
    }
}
